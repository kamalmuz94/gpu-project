#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
unsigned char mul(unsigned char a, unsigned char b);
unsigned char* shift_row(unsigned char *input);
unsigned char s_box(unsigned char a);
unsigned char * mix_column(unsigned char * input);
unsigned char* shift_rowI(unsigned char *input);
unsigned char s_boxI(unsigned char a);
unsigned char * mix_columnI(unsigned char * input);
__global__ void kernel_s_box(unsigned char *s_box,unsigned char *data,int limit)
{
	int tx =  blockIdx.x * blockDim.x +threadIdx.x;
	if(tx<limit)
	{
	int row = data[tx]>>4;
	int col = data[tx]&0x0f;
	data[tx] = s_box[row*16 + col];
	}
}
__global__ void kernel_s_boxI(unsigned char *s_box,unsigned char *data,int limit)
{
	int tx =  blockIdx.x * blockDim.x +threadIdx.x;
	if(tx<limit)
	{
		int row = data[tx]>>4;
		int col = data[tx]&0x0f;
		data[tx] = s_box[row*16 + col];
	}
}


__global__ void kernel_shift_row(unsigned char *data,int limit)
{
	int tx =  blockIdx.x * blockDim.x +threadIdx.x;
	if(tx<limit)
	{
		unsigned char temp = data[tx];
		int shift = (threadIdx.x%16) / 4;
		int shift1 = 4 - shift;
		__syncthreads();
		int i = threadIdx.x;
		i = ((i+shift1) % 4)+4*shift;
		data[blockIdx.x*blockDim.x +(threadIdx.x/16)*16 + i] = temp;
	}
}

__global__ void kernel_shift_rowI(unsigned char *data,int limit)
{
	int tx =  blockIdx.x * blockDim.x+threadIdx.x;
	if(tx<limit)
	{
		unsigned char temp = data[tx];
		int shift = (threadIdx.x%16) / 4;
		__syncthreads();
		int i = threadIdx.x;
		i = ((i+shift) % 4)+4*shift;
		data[blockIdx.x*blockDim.x +(threadIdx.x/16)*16 + i] = temp;
	}
}

//======================================matrix multiply=======================================================
__global__ void kernel_mix_column(unsigned char *d_data,unsigned char *rot_matrix,int limit)
{
	int col = threadIdx.x % 4;
	int row = (threadIdx.x%16) / 4;
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	if(tx<limit)
	{
		extern __shared__ unsigned char data[];
		data[threadIdx.x] = d_data[tx];
		__syncthreads();
		int i,j;	
		unsigned char a,b;
		unsigned short c = 0,d;
		int pow = 128;
		short x = 0x11b;
		unsigned char temp;
		for (int i =0; i < 4; ++i)
		{
	//		b = ds_A[row][i];
	//		a = ds_B[i][col];
			a = rot_matrix[row*4 + i];		
			b = data[(threadIdx.x/16 * 16)+i*4 + col];
			pow = 128;
			c = 0;
			for(j = 7;j>=0;j--)
			{
				if(pow & a)
				{
					d = b<<j;
					c = c^d;
				}	
				pow = pow>>1;
			}
			unsigned short y = 0x4000;
			for(j = 14;j>=8;j--)
			{
				if(y & c)
					c = (x<<(j-8)) ^ c;
				y = y>>1;
			}
			temp  = temp^c;
		}
		data[(threadIdx.x/16 * 16)+row*4 + col] = temp;
		__syncthreads();
		d_data[tx]=data[threadIdx.x];
	}
}

//============================================================================================================

__global__ void kernal_key_exapnsion(unsigned char *d_sbox ,unsigned char *d_add_rnd_key)
{
	 int tx = threadIdx.x;
    __shared__ unsigned char temp[4],temp1[4];
	__shared__ unsigned char d_Rcon[10];
	d_Rcon[0]=0x01;
	d_Rcon[1]=0x02;
	d_Rcon[2]=0x04;
	d_Rcon[3]=0x08;
	d_Rcon[4]=0x10;
	d_Rcon[5]=0x20;
	d_Rcon[6]=0x40;
	d_Rcon[7]=0x80;
	d_Rcon[8]=0x1B;
	d_Rcon[9]=0x36;
    __shared__ unsigned char d_rcon[4]; 
	d_rcon[0]=0x01;
	d_rcon[1]=0x00;
	d_rcon[2]=0x00;
	d_rcon[3]=0x00;
    int round,count;
    count = 16;
    for(round=1;round<=10;round++)
    {
	if(tx<4){
	        temp[tx]=d_add_rnd_key[round*16 - 4 +tx];
	        temp1[tx] = temp[(tx+1)%4];
	        temp1[tx] = d_sbox[(temp1[tx]>>4)*16 + (temp1[tx]&0x0f)];
	        d_rcon[0]=d_Rcon[round-1];
	        temp1[0] = temp1[0]^d_rcon[0];
	}
	__syncthreads();
          
            if(tx<4){
                    d_add_rnd_key[count+tx]= temp1[tx]^d_add_rnd_key[round*16-16+tx];
	}
            else if(tx>3 && tx<8)
                    d_add_rnd_key[count+tx]=temp1[tx%4]^d_add_rnd_key[round*16-16+tx%4]^d_add_rnd_key[round*16-12+tx%4];
            else if(tx>7 && tx<12)
                    d_add_rnd_key[count+tx]=temp1[tx%4]^d_add_rnd_key[round*16-16+tx%4]^d_add_rnd_key[round*16-12+tx%4]^d_add_rnd_key[round*16-8+tx%4];
            else 
                    d_add_rnd_key[count+tx]=temp1[tx%4]^d_add_rnd_key[round*16-16+tx%4]^d_add_rnd_key[round*16-12+tx%4]^d_add_rnd_key[round*16-8+tx%4]^d_add_rnd_key[round*16-4+tx%4];
            count+=16;
            __syncthreads();
    }
}

__global__ void kernal_Add_round_key(unsigned char *d_data,unsigned char *d_add_rnd_key,int count,int limit)
{
	int tx = blockIdx.x *blockDim.x +threadIdx.x;
	if(tx<limit)
		d_data[tx] = d_data[tx]^d_add_rnd_key[count+threadIdx.x%16];
}
__global__ void kernal_I_Add_round_key(unsigned char *d_data,unsigned char *d_add_rnd_key,int count,int limit)
{
	int tx =  blockIdx.x * blockDim.x +threadIdx.x;
	if(tx<limit)
		d_data[tx]=d_data[tx]^d_add_rnd_key[count + threadIdx.x%16 -16];
}


int main(int argc,char *argv[])
{
	int i,j,s;
	int no_blocks;

	FILE *filePointer;
    	char ch1;
	int no_char=0;
	char *input_file = "SampleTextFile_1000kb.txt";
	
    	filePointer = fopen(input_file, "r");
    	if (filePointer == NULL)
        printf("File is not available \n");
    	else
        	while ((ch1 = fgetc(filePointer)) != EOF)
            		no_char++;
    	fclose(filePointer);
	no_blocks = (no_char+15)/16;
	int extra = no_blocks * 16 - no_char;
	printf("no_blocks :: %d\n",no_blocks);

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float total_time = 0;	
	unsigned char  rot_mat[16] = {0x02,0x03,0x01,0x01,   0x01,0x02,0x03,0x01,   0x01,0x01,0x02,0x03,   0x03,0x01,0x01,0x02};
	unsigned char  irot_mat[16] = {0x0e,0x0b,0x0d,0x09,   0x09,0x0e,0x0b,0x0d,   0x0d,0x09,0x0e,0x0b,   0x0b,0x0d,0x09,0x0e};
	unsigned char *data = (unsigned char*)malloc(16*no_blocks*sizeof(unsigned char));
	char *key = argv[1];
    	unsigned char add_rnd_key[176];
    	for(int i=0;i<176;i++)
        	add_rnd_key[i]=48;
   	for(int i=0;(key[i]!='\0' && i<16);i++)
        	add_rnd_key[i]=key[i];

	char s_table[256] = {
		0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x1, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 
		0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 
		0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 
		0x4, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x5, 0x9a, 0x7, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 
		0x9, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 
		0x53, 0xd1, 0x0, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 
		0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x2, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 
		0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 
		0xcd, 0xc, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 
		0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0xb, 0xdb, 
		0xe0, 0x32, 0x3a, 0xa, 0x49, 0x6, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 
		0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x8, 
		0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 
		0x70, 0x3e, 0xb5, 0x66, 0x48, 0x3, 0xf6, 0xe, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 
		0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 
		0x8c, 0xa1, 0x89, 0xd, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0xf, 0xb0, 0x54, 0xbb, 0x16, 
		};
	char is_table[256] = {
		0x52, 0x9, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb, 
		0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, 
		0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0xb, 0x42, 0xfa, 0xc3, 0x4e, 
		0x8, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 
		0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 
		0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 
		0x90, 0xd8, 0xab, 0x0, 0x8c, 0xbc, 0xd3, 0xa, 0xf7, 0xe4, 0x58, 0x5, 0xb8, 0xb3, 0x45, 0x6, 
		0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0xf, 0x2, 0xc1, 0xaf, 0xbd, 0x3, 0x1, 0x13, 0x8a, 0x6b, 
		0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73, 
		0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e, 
		0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0xe, 0xaa, 0x18, 0xbe, 0x1b, 
		0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 
		0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x7, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 
		0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0xd, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 
		0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 
		0x17, 0x2b, 0x4, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0xc, 0x7d, 
		};

	unsigned char *d_add_rnd_key;
    	cudaMalloc(&d_add_rnd_key,176*sizeof(unsigned char));
    	cudaMemcpy(d_add_rnd_key,add_rnd_key,176*sizeof(unsigned char),cudaMemcpyHostToDevice);
	unsigned char * d_s_table,* i_d_s_table, *d_data,*h_data = (unsigned char*)malloc(16*no_blocks*sizeof(unsigned char)),*d_rot_mat,*d_irot_mat;
	cudaMalloc(&d_rot_mat,16*sizeof(unsigned char));
	cudaMalloc(&d_irot_mat,16*sizeof(unsigned char));
	cudaMalloc(&d_data,16*no_blocks*sizeof(unsigned char));
	cudaMalloc(&d_s_table,256*sizeof(unsigned char));
	cudaMalloc(&i_d_s_table,256*sizeof(unsigned char));

	cudaMemcpy(d_s_table,s_table,256*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(i_d_s_table,is_table,256*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_rot_mat,rot_mat,16*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_irot_mat,irot_mat,16*sizeof(unsigned char),cudaMemcpyHostToDevice);
	int r;
	//===========================key Expansion kernal call======================================
	kernal_key_exapnsion<<<1,16>>>(d_s_table,d_add_rnd_key);
	//==========================Encryption======================================================
	FILE *in = fopen(input_file,"r");
	FILE *out = fopen("encrypted_file.txt","w");	
		

	for(s = 0;s<no_blocks;s++)
	{
		for(r = 0;r<16;r++)
		{
			if((s*16+r)<no_char)
			fscanf(in,"%c",&data[s*16+r]);
			else
			data[s*16+r]='0';
		}
	}
	cudaEventRecord(start);
	cudaMemcpy(d_data,data,16*no_blocks*sizeof(unsigned char),cudaMemcpyHostToDevice);
	int count=0;

	int blockpergrid,zz;
	int threadperblock;
//===================================================== No of thread per block =====================================================	
	threadperblock=256;
	printf("thread per block %d\n",threadperblock);
//==================================================================================================================================
	zz=16;
	zz=threadperblock/zz;
	blockpergrid = (int)ceil(1.0*no_blocks/zz);
	int limit=no_blocks*16;
	kernal_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit);	//<--------
	count+=16;
	for(i = 0;i<9;i++)
	{
		kernel_s_box<<<blockpergrid,threadperblock>>>(d_s_table,d_data,limit);
		kernel_shift_row<<<blockpergrid,threadperblock>>>(d_data,limit);
		kernel_mix_column<<<blockpergrid,threadperblock,threadperblock>>>(d_data,d_rot_mat,limit);	
		kernal_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit);
		count+=16;
	}
	kernel_s_box<<<blockpergrid,threadperblock>>>(d_s_table,d_data,limit);
	kernel_shift_row<<<blockpergrid,threadperblock>>>(d_data,limit);
	kernal_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit); 	//<--------
	cudaMemcpy(h_data,d_data,16*no_blocks*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&total_time, start, stop);
	printf("Encryption: input size : %d, Time %f\n",no_blocks,total_time/1000);	
	for(i=0;i<no_blocks;i++)
	{
		for(j = 0;j<16;j++)
		{
				fprintf(out,"%c",h_data[i*16+j]);
		}
	}
	fclose(in);
	fclose(out);
	//=================================Decryption=================================================
	in = fopen("encrypted_file.txt","r");
	out = fopen("decrypted_file.txt","w");
	for(s = 0;s<no_blocks;s++)
	{
		for(r = 0;r<16;r++)
			fscanf(in,"%c",&data[s*16+r]);
	}
	cudaEventRecord(start);
	cudaMemcpy(d_data,data,16*no_blocks*sizeof(unsigned char),cudaMemcpyHostToDevice);
	count=176;
	kernal_I_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit);	//<--------
	kernel_shift_rowI<<<blockpergrid,threadperblock>>>(d_data,limit);
	kernel_s_boxI<<<blockpergrid,threadperblock>>>(i_d_s_table,d_data,limit);
	count-=16;
	for(i = 0;i<9;i++)
	{
		kernal_I_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit);
		kernel_mix_column<<<blockpergrid,threadperblock,threadperblock>>>(d_data,d_irot_mat,limit);	
		kernel_shift_rowI<<<blockpergrid,threadperblock>>>(d_data,limit);
		kernel_s_boxI<<<blockpergrid,threadperblock>>>(i_d_s_table,d_data,limit);
		count-=16;
	}
	kernal_I_Add_round_key<<<blockpergrid,threadperblock>>>(d_data,d_add_rnd_key,count,limit);   //<--------
	cudaMemcpy(h_data,d_data,16*no_blocks*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&total_time, start, stop);
	printf("Decryption: input size : %d, Time %f\n",no_blocks,total_time/1000);
	for(i=0;i<no_blocks;i++){
		for(j = 0;j<16;j++)
		{
			if((i*16+j)<no_char)
				fprintf(out,"%c",h_data[i*16+j]);
		}
	}
	
	//================================================================================================
	fclose(in);
	fclose(out);
	printf("\n");
return 0;
}

