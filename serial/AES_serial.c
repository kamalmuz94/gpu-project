#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h> 
unsigned char mul(unsigned char a, unsigned char b);
unsigned char* shift_row(unsigned char *input);
unsigned char s_box(unsigned char a);
unsigned char * mix_column(unsigned char * input);
unsigned char* shift_rowI(unsigned char *input);
unsigned char s_boxI(unsigned char a);
unsigned char * mix_columnI(unsigned char * input);
void key_expansion(unsigned char *key,unsigned char **result,char s_box[]);
void rotword(unsigned char a[],char s_box[]);
unsigned char Rcon[10]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80,0x1B,0x36};
unsigned char rcon[4] = {0x01,0x00,0x00,0x00};
int main()
{
	clock_t t,t1=0.0;
	int i,j;
	int data_size = 2048;
	FILE *filePointer;
    	char ch1;
	int no_char=0;
    	filePointer = fopen("5MB_file.txt", "r");
    	if (filePointer == NULL)
        printf("File is not available \n");
    	else
        	while ((ch1 = fgetc(filePointer)) != EOF)
            		no_char++;
    	fclose(filePointer);

	data_size = (no_char+15)/16;
	printf("data_size :: %d\n",data_size);

	unsigned char  rot_mat[16] = {0x02,0x03,0x01,0x01,   0x01,0x02,0x03,0x01,   0x01,0x01,0x02,0x03,   0x03,0x01,0x01,0x02};
	unsigned char *data = (unsigned char*)malloc(16*sizeof(unsigned char));
	unsigned char *data1 = (unsigned char*)malloc(16*sizeof(unsigned char));
	//======kamal========
	unsigned char *key=(unsigned char *)malloc(16*sizeof(unsigned char ));
	//int *convert_key=(int *)malloc(16*sizeof(int));

	unsigned char **add_round_key = (unsigned char **)malloc(44*sizeof(unsigned char *));
	for( i=0;i<44;i++)
	    add_round_key[i]=(unsigned char *)malloc(4*sizeof(unsigned char));
	    
	for( i=0;i<16;i++)
	   key[i]='0';
	printf("entre the key::\n");
	scanf("%s",key);
	//key[0]=0x68;
	//key[1]=0x65;
	//key[2]=0x6c;
	//key[3]=0x6c;
	//key[4]=0x6f;
	//=======kamal=========	
char s_table[256] = {
		0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x1, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 
		0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 
		0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 
		0x4, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x5, 0x9a, 0x7, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 
		0x9, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 
		0x53, 0xd1, 0x0, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 
		0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x2, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 
		0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 
		0xcd, 0xc, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 
		0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0xb, 0xdb, 
		0xe0, 0x32, 0x3a, 0xa, 0x49, 0x6, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 
		0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x8, 
		0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 
		0x70, 0x3e, 0xb5, 0x66, 0x48, 0x3, 0xf6, 0xe, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 
		0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 
		0x8c, 0xa1, 0x89, 0xd, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0xf, 0xb0, 0x54, 0xbb, 0x16, 
		};
char is_table[256] = {
		0x52, 0x9, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb, 
		0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, 
		0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0xb, 0x42, 0xfa, 0xc3, 0x4e, 
		0x8, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 
		0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 
		0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 
		0x90, 0xd8, 0xab, 0x0, 0x8c, 0xbc, 0xd3, 0xa, 0xf7, 0xe4, 0x58, 0x5, 0xb8, 0xb3, 0x45, 0x6, 
		0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0xf, 0x2, 0xc1, 0xaf, 0xbd, 0x3, 0x1, 0x13, 0x8a, 0x6b, 
		0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73, 
		0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e, 
		0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0xe, 0xaa, 0x18, 0xbe, 0x1b, 
		0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 
		0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x7, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 
		0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0xd, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 
		0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 
		0x17, 0x2b, 0x4, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0xc, 0x7d, 
		};
t = clock(); 
	key_expansion(key,add_round_key,s_table);
t1 = t1+clock() - t;
	unsigned char *Key = (unsigned char *)malloc(176*sizeof(unsigned char));
	int count = 0;
	for(i =0;i<44;i++)
	{	
		for(j = 0;j<4;j++)
		{
			Key[count] = add_round_key[i][j];
			count++;
		}
	}

	int k,s,z;
	
	FILE *in,*out;
	in = fopen("Input.txt","r");
	out = fopen("encrypted_file.txt","w");	
	count = 0;
 	
//t = clock(); 	
//========================================================================================================
	for(s = 0;s<data_size;s++)
	{
		count = 0;
//================================== take input =========================================================
		for(i =0;i<16;i++)
			fscanf(in,"%c",data+i);
//====================================== encrypt ========================================================
t = clock(); 
		for(z = 0;z<16;z++)
			data[z] = data[z]^Key[count++];			//add round key
			
		for(i = 0;i<9;i++)
		{
	
			for(j = 0;j<16;j++){
				//data[j] = s_box(data[j]);
				int roww;
				int coll;
				roww = data[j]>>4;
				coll = data[j]&0x0f;
				data[j] = s_table[roww*16 +coll];
			}			//substitute byte for each byte of data
			data = 	shift_row(data);			//shift row
			data = mix_column(data);			//mix column
			for(z = 0;z<16;z++)
				data[z] = data[z]^Key[count++];			//add round key
		}
		for(j = 0;j<16;j++){
				//data[j] = s_box(data[j]);
			int roww;
			int coll;
			roww = data[j]>>4;
			coll = data[j]&0x0f;
			data[j] = s_table[roww*16 +coll];
		}
		data = 	shift_row(data);			//shift row
		for(z = 0;z<16;z++)
			data[z] = data[z]^Key[count++];			//add round key
t1 = t1+clock() - t;
//=================================== print to file =========================================================
		for(i =0;i<16;i++)
		{
			fprintf(out,"%c",data[i]);
		}
	}
//============================================================================================================	
//t = clock() - t; 
double time_taken = ((double)t1)/CLOCKS_PER_SEC;	
	printf("Encryption: input sizd %d, time: %lf\n",data_size,time_taken);

	
	fclose(in);
	fclose(out);
	in = fopen("encrypted_file.txt","r");
	out = fopen("decrypted_file.txt","w");

t1=0.0;
//t = clock(); 	
//============================================================================================================	
	for(s = 0;s<data_size;s++)
	{
		count = 176;
//====================================== take input ==========================================================
		for(i =0;i<16;i++)
			fscanf(in,"%c",data+i);
//======================================== decrypt ===========================================================
t = clock(); 
		for(z = 15;z>=0;z--)				//add round key
			data[z] = data[z]^Key[--count];	
		data = 	shift_rowI(data);			//shift row			
		for(j = 0;j<16;j++){
				//data[j] = s_box(data[j]);
			int roww;
			int coll;
			roww = data[j]>>4;
			coll = data[j]&0x0f;
			data[j] = is_table[roww*16 +coll];
		}		
		for(i = 0;i<9;i++)
		{
			for(z = 15;z>=0;z--)				//add round key
				data[z] = data[z]^Key[--count];				
			data = mix_columnI(data);			//mix column	
			data = 	shift_rowI(data);			//shift row		
			for(j = 0;j<16;j++){
				//data[j] = s_box(data[j]);
				int roww;
				int coll;
				roww = data[j]>>4;
				coll = data[j]&0x0f;
				data[j] = is_table[roww*16 +coll];
			}			}
		for(z = 15;z>=0;z--)				//add round key
			data[z] = data[z]^Key[--count];
t1 = t1 + clock() - t;
//==================================== print to file =========================================================				
		for(i =0;i<16;i++)
		{
			fprintf(out,"%c",data[i]);
		}
	}	
//=============================================================================================================
//t = clock() - t; 
time_taken = ((double)t1)/CLOCKS_PER_SEC;	
	printf("Decryption : input sizd %d, time: %lf\n",data_size,time_taken);	
	fclose(in);
	fclose(out);
return 0;
}
unsigned char * mix_column(unsigned char*input)
{
	unsigned char  rot_mat[16] = {0x02,0x03,0x01,0x01,   0x01,0x02,0x03,0x01,   0x01,0x01,0x02,0x03,   0x03,0x01,0x01,0x02};
	unsigned char *output_mat = (unsigned char*)malloc(16*sizeof(unsigned char));
	int i,j,k;
	for(i = 0;i<4;i++)
		for(j = 0;j<4;j++)
			for(k = 0;k<4;k++)
				output_mat[i*4 + j]  ^= mul(rot_mat[i*4+k] , input[k*4+j]);
return (output_mat);
}
unsigned char * mix_columnI(unsigned char*input)
{
	unsigned char  rot_mat[16] = {0x0e,0x0b,0x0d,0x09,   0x09,0x0e,0x0b,0x0d,   0x0d,0x09,0x0e,0x0b,   0x0b,0x0d,0x09,0x0e};
	unsigned char *output_mat = (unsigned char*)malloc(16*sizeof(unsigned char));
	int i,j,k;
	for(i = 0;i<4;i++)
		for(j = 0;j<4;j++)
			for(k = 0;k<4;k++)
				output_mat[i*4 + j]  ^= mul(rot_mat[i*4+k] , input[k*4+j]);
return (output_mat);
}


unsigned char* shift_row(unsigned char *input)
{
	unsigned char * output = (unsigned char*)malloc(16*sizeof(char));
	output[0] = input[0];
	output[1] = input[1];
	output[2] = input[2];
	output[3] = input[3];
	output[4] = input[5];
	output[5] = input[6];
	output[6] = input[7];
	output[7] = input[4];
	output[8] = input[10];
	output[9] = input[11];
	output[10] = input[8];
	output[11] = input[9];
	output[12] = input[15];
	output[13] = input[12];
	output[14] = input[13];
	output[15] = input[14];
	return output;
}
unsigned char* shift_rowI(unsigned char *input)
{
	unsigned char * output = (unsigned char*)malloc(16*sizeof(char));
	output[0] = input[0];
	output[1] = input[1];
	output[2] = input[2];
	output[3] = input[3];
	output[4] = input[7];
	output[5] = input[4];
	output[6] = input[5];
	output[7] = input[6];
	output[8] = input[10];
	output[9] = input[11];
	output[10] = input[8];
	output[11] = input[9];
	output[12] = input[13];
	output[13] = input[14];
	output[14] = input[15];
	output[15] = input[12];
	return output;
}



unsigned char mul(unsigned char a, unsigned char b)
{
	short c = 0,d;
	int i,pow = 128;
	short x = 0x11b;
	for(i = 7;i>=0;i--)
	{
		if(pow & b)
		{
			d = a<<i;
			c = c^d;
		}	
		pow = pow>>1;
	}
	unsigned short y = 0x4000;
	for(i = 14;i>=8;i--)
	{
		if(y & c)
			c = (x<<(i-8)) ^ c;
		y = y>>1;
	}
	unsigned char R = c;
	return R;
} 
	
void rotword(unsigned char a[],char s_box[])
{
    static int i=0;
    unsigned char z=a[0];
    a[0]=a[1];
    a[1]=a[2];
    a[2]=a[3];
    a[3]=z;
    int row,col;
    row=a[0]>>4;
    col=a[0]&0x0f;
    a[0]=s_box[row*16+col];
	row=a[1]>>4;
	col=a[1]&0x0f;
    a[1]=s_box[row*16+col];
	row=a[2]>>4;
	col=a[2]&0x0f;
    a[2]=s_box[row*16+col];
	row=a[3]>>4;
	col=a[3]&0x0f;
    a[3]=s_box[row*16+col];
    a[0]=a[0]^Rcon[i];
    i++;
}
void key_expansion(unsigned char *key,unsigned char **result,char s_box[])
{
    int i,j,k;
    unsigned char arr[4];
    unsigned char brr[4];
	for(i=0;i<4;i++)
	    for(j=0;j<4;j++)
	        result[i][j]=key[i*4+j];
	for(i=0;i<10;i++)
	{
		for( k=0;k<4;k++)
		{
			arr[k] = result[i*4+0][k];
			brr[k] = result[i*4+3][k];
		}
		rotword(brr,s_box);
        for( k=0;k<4;k++)
			result[(i+1)*4+0][k]=arr[k]^brr[k];
		for( j=1;j<4;j++)
			for( k=0;k<4;k++)
				result[(i+1)*4+j][k]= result[(i+1)*4+j-1][k]^result[i*4+j][k];
	}
}
